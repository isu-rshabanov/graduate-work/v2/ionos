pub const DEFAULT_KP_THRESHOLD: f64 = 2.67;

#[derive(Debug, Clone, PartialEq)]
pub struct F10_7 {
    pub obs: f64,
    pub adj: f64,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Bsr {
    pub value: i32,
    pub day: i32,
}
