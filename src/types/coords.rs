pub type Degree = f64;
pub type Altitude = f64;

/// Geodesic coordinate type
/// note: altitude is calculated in meters
#[derive(Debug, Clone, PartialEq)]
pub struct GeoCoords {
    pub lat: Degree,
    pub lon: Degree,
    pub alt: Altitude,
}

impl GeoCoords {
    pub fn new(lat: Degree, lon: Degree, alt: Altitude) -> Self {
        Self { lat, lon, alt }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct CelestialCoords {
    pub azimuth: Degree,
    pub elevation: Degree,
}
