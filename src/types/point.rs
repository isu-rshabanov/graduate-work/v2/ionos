use std::ops::{Add, Sub};

use crate::{
    earth_consts,
    utils::{to_kilometers, to_meters},
};

use super::coords::{Degree, GeoCoords};

#[derive(Debug, Clone, PartialEq)]
pub struct Point {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Point {
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Self { x, y, z }
    }

    /// Computes XYZ coordinates from altitude (elevation), azimuth and radius-vector (in kilometers)
    pub fn with_altazr(&self, mut alt: Degree, mut az: Degree, r: f64) -> Self {
        alt = alt.to_radians();
        az = az.to_radians();

        let (lat, lon) = self.to_latlon_radians();

        let delt = (alt.sin() * lat.sin() + az.cos() * alt.cos() * lat.cos()).asin();
        let th = -(-az.sin() * alt.cos())
            .atan2(alt.sin() * lat.cos() - az.cos() * alt.cos() * lat.sin());

        Self {
            x: self.x + r * delt.cos() * (lon + th).cos(),
            y: self.y + r * delt.cos() * (lon + th).sin(),
            z: self.z + r * delt.sin(),
        }
    }

    fn to_latlon_radians(&self) -> (f64, f64) {
        let coords: GeoCoords = self.into();
        (coords.lat.to_radians(), coords.lon.to_radians())
    }

    /// Converts XYZ coordinates to altitude (elevation), azimuth and radius-vector
    pub fn to_altazr(&self, other: &Point) -> (Degree, Degree, f64) {
        let (dx, dy, dz) = (other.x - self.x, other.y - self.y, other.z - self.z);
        let r = (dx.powi(2) + dy.powi(2) + dz.powi(2)).sqrt();

        let (lat, lon) = self.to_latlon_radians();

        let delt = (dz / r).asin();
        let th = lon - dy.atan2(dx);

        let alt = std::f64::consts::FRAC_PI_2
            - (delt.sin() * lat.sin() + lat.cos() * delt.cos() * th.cos()).acos();

        let x = lat.sin() * delt.cos() * th.cos() - lat.cos() * delt.sin();
        let y = delt.cos() * th.sin();

        let mut az = (-y).atan2(-x);
        az = if az.is_sign_negative() {
            az + std::f64::consts::TAU
        } else {
            az
        };

        (alt.to_degrees(), az.to_degrees(), r)
    }

    pub fn mag(&self) -> f64 {
        (self.x.powi(2) + self.y.powi(2) + self.z.powi(2)).sqrt()
    }
}

impl From<GeoCoords> for Point {
    fn from(coords: GeoCoords) -> Self {
        Point::from(&coords)
    }
}

impl From<&GeoCoords> for Point {
    fn from(coords: &GeoCoords) -> Self {
        let lat = coords.lat.to_radians();
        let lon = coords.lon.to_radians();
        let alt = to_kilometers(coords.alt);

        let t = earth_consts::SEMI_MAJOR_AXIS
            / (1.0 - earth_consts::ECCENTRICITY_SQUARE * lat.sin() * lat.sin()).sqrt();

        Self {
            x: (t + alt) * lat.cos() * lon.cos(),
            y: (t + alt) * lat.cos() * lon.sin(),
            z: (t * (1.0 - earth_consts::ECCENTRICITY_SQUARE) + alt) * lat.sin(),
        }
    }
}

impl From<Point> for GeoCoords {
    fn from(point: Point) -> Self {
        GeoCoords::from(&point)
    }
}

impl From<&Point> for GeoCoords {
    fn from(point: &Point) -> GeoCoords {
        let r = (point.x.powi(2) + point.y.powi(2)).sqrt();

        let lon = point.y.atan2(point.x);
        let mut lat = (point.z / r).atan();
        let mut alt;

        let mut c;
        for _ in 0..15 {
            c = (1.0 - earth_consts::ECCENTRICITY_SQUARE * lat.sin() * lat.sin())
                .sqrt()
                .recip();
            alt = point.z
                + earth_consts::SEMI_MAJOR_AXIS * c * earth_consts::ECCENTRICITY_SQUARE * lat.sin();
            lat = (alt / r).atan();
        }

        c = (1.0 - earth_consts::ECCENTRICITY_SQUARE * lat.sin() * lat.sin())
            .sqrt()
            .recip();
        alt = to_meters(r / lat.cos() - earth_consts::SEMI_MAJOR_AXIS * c);

        GeoCoords {
            lat: lat.to_degrees(),
            lon: lon.to_degrees(),
            alt,
        }
    }
}

impl Sub for Point {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}
