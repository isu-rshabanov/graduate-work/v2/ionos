#[derive(Debug, Clone, PartialEq)]
pub struct TecData {
    pub roti: f64,
    pub tec: f64,
    pub dtec_2_10: f64,
    pub dtec_10_20: f64,
    pub dtec_20_60: f64,
    pub tec_adjusted: Option<f64>,
}
