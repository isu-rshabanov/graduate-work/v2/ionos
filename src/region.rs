use crate::types::coords::{Altitude, Degree, GeoCoords};

const DEFAULT_ALTITUDE: Altitude = 0.0;

pub trait Region {
    fn contains(&self, coords: &GeoCoords) -> bool;
}

#[derive(Debug, Clone, PartialEq)]
pub struct RectRegion {
    pub lt: GeoCoords,
    pub rb: GeoCoords,
}

impl RectRegion {
    pub fn new(lt: GeoCoords, rb: GeoCoords) -> Self {
        Self { lt, rb }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct EllipseRegion {
    pub center: GeoCoords,
    pub lon_radius: Degree,
    pub lat_radius: Degree,
}

impl EllipseRegion {
    pub fn new(center: GeoCoords, lat_radius: Degree, lon_radius: Degree) -> Self {
        Self {
            center,
            lon_radius,
            lat_radius,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct RectEllipseRegion {
    pub rect: RectRegion,
    pub ellipse: EllipseRegion,
}

impl From<RectRegion> for RectEllipseRegion {
    fn from(region: RectRegion) -> Self {
        Self::from(&region)
    }
}

impl From<&RectRegion> for RectEllipseRegion {
    fn from(region: &RectRegion) -> Self {
        let center = GeoCoords::new(
            (region.lt.lat + region.rb.lat) / 2.0,
            (region.lt.lon + region.rb.lon) / 2.0,
            DEFAULT_ALTITUDE,
        );

        let lon_radius = (region.rb.lon - center.lon) / 3.0;
        let lat_radius = (region.rb.lat - center.lat) / 3.0;

        let rect = RectRegion {
            lt: GeoCoords::new(
                center.lat - lat_radius,
                center.lon - lon_radius,
                DEFAULT_ALTITUDE,
            ),
            rb: GeoCoords::new(
                center.lat + lat_radius,
                center.lon + lon_radius,
                DEFAULT_ALTITUDE,
            ),
        };

        let ellipse = EllipseRegion::new(center, lat_radius, lon_radius);

        Self { rect, ellipse }
    }
}

impl Region for RectRegion {
    fn contains(&self, point: &GeoCoords) -> bool {
        point.lat >= self.lt.lat
            && point.lat <= self.rb.lat
            && point.lon >= self.lt.lon
            && point.lon <= self.rb.lon
    }
}

impl Region for EllipseRegion {
    fn contains(&self, point: &GeoCoords) -> bool {
        let x = ((point.lon - self.center.lon) / self.lon_radius).powi(2);
        let y = ((point.lat - self.center.lat) / self.lat_radius).powi(2);

        x + y <= 1.0
    }
}

impl Region for RectEllipseRegion {
    fn contains(&self, coords: &GeoCoords) -> bool {
        self.rect.contains(coords) && self.ellipse.contains(coords)
    }
}
