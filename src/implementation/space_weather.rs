use std::{
    collections::BTreeMap,
    ops::{Bound, RangeBounds},
};

use chrono::{Duration, NaiveDateTime};

pub fn get_kp_range<R>(
    kps: &BTreeMap<NaiveDateTime, f64>,
    range: R,
) -> std::collections::btree_map::Range<NaiveDateTime, f64>
where
    R: RangeBounds<NaiveDateTime>,
{
    kps.range(get_time_range(range))
}

pub fn get_time_range<R: RangeBounds<NaiveDateTime>>(
    range: R,
) -> (Bound<NaiveDateTime>, Bound<NaiveDateTime>) {
    let kp_time_step = Duration::hours(3).num_seconds();

    let start: Bound<NaiveDateTime> = match range.start_bound() {
        Bound::Included(v) => {
            let timestamp = v.timestamp();

            Bound::Included(
                NaiveDateTime::from_timestamp_opt(timestamp - timestamp % kp_time_step, 0).unwrap(),
            )
        }
        Bound::Excluded(v) => {
            let timestamp = v.timestamp();

            Bound::Included(
                NaiveDateTime::from_timestamp_opt(timestamp - timestamp % kp_time_step, 0).unwrap(),
            )
        }
        Bound::Unbounded => Bound::Unbounded,
    };

    (start, range.end_bound().cloned())
}
