use std::ops::RangeBounds;

use crate::traits::iss::{
    traces::{IssTraces, RegionTraces, Traces},
    IssRecord,
};

use super::space_weather::get_kp_range;

pub struct Iss<T: IssRecord> {
    records: Vec<T>,
}

impl<T: IssRecord> Iss<T> {
    pub fn range<R>(&self, range: R) -> impl Iterator<Item = &T> + '_
    where
        R: RangeBounds<chrono::NaiveDateTime>,
    {
        let start_idx = match range.start_bound() {
            std::ops::Bound::Included(start) => {
                self.records.partition_point(|x| x.get_datetime() < start)
            }
            std::ops::Bound::Excluded(start) => {
                self.records.partition_point(|x| x.get_datetime() <= start)
            }
            std::ops::Bound::Unbounded => 0,
        };

        let end_idx = match range.end_bound() {
            std::ops::Bound::Included(end) => {
                self.records[start_idx..].partition_point(|x| x.get_datetime() <= end)
            }
            std::ops::Bound::Excluded(end) => {
                self.records[start_idx..].partition_point(|x| x.get_datetime() < end)
            }
            std::ops::Bound::Unbounded => self.records.len(),
        };

        let indices = if start_idx == end_idx {
            start_idx..end_idx
        } else {
            start_idx..start_idx + end_idx
        };

        self.records[indices].iter()
    }

    pub fn records(&self) -> impl Iterator<Item = &T> + '_ {
        self.records.iter()
    }
}

impl<T: IssRecord> FromIterator<T> for Iss<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let mut records = Vec::from_iter(iter);
        records.sort_by(|lhs, rhs| lhs.get_datetime().cmp(rhs.get_datetime()));

        Self { records }
    }
}

impl<T: IssRecord> IntoIterator for Iss<T> {
    type Item = T;

    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.records.into_iter()
    }
}

impl<T: IssRecord> Traces<T> for Iss<T> {
    fn traces(&self) -> IssTraces<T> {
        let mut traces = Vec::default();
        let mut records = self.records.iter();

        if let Some(record) = records.next() {
            let mut dt = record.get_datetime();
            let mut start_idx = 0;

            for (i, record) in records.enumerate() {
                if !self.are_continuous_records(record.get_datetime(), dt) {
                    traces.push(
                        self.records[start_idx..i]
                            .iter()
                            .collect::<Vec<_>>()
                            .into_iter(),
                    );
                    start_idx = i;
                }

                dt = record.get_datetime();
            }
            traces.push(
                self.records[start_idx..]
                    .iter()
                    .collect::<Vec<_>>()
                    .into_iter(),
            );
        }

        IssTraces {
            traces: traces.into_iter(),
        }
    }
}

impl<T> RegionTraces<T> for Iss<T>
where
    T: IssRecord + Clone,
{
    fn region_traces(
        &self,
        region: &impl crate::region::Region,
        kps: &Option<std::collections::BTreeMap<chrono::NaiveDateTime, f64>>,
        max_kp: f64,
    ) -> IssTraces<T> {
        let traces = self
            .traces()
            .filter(|trace| {
                trace
                    .clone()
                    .any(|record| region.contains(record.get_geo_coords()))
            })
            .fold(Vec::default(), |mut acc, trace| {
                if let (Some(first), Some(last)) = (trace.clone().next(), trace.clone().last()) {
                    let iter = kps.as_ref().map_or_else(
                        || trace.clone().collect::<Vec<_>>().into_iter(),
                        |kps| {
                            get_kp_range(kps, first.get_datetime()..last.get_datetime())
                                .filter(|(_, kp)| **kp <= max_kp)
                                .fold(Vec::default(), |mut records, (datetime, _)| {
                                    let datetime = datetime
                                        .checked_add_signed(chrono::Duration::hours(3))
                                        .unwrap();

                                    for record in trace.clone().skip(records.len()) {
                                        if datetime.gt(record.get_datetime()) {
                                            records.push(record);
                                        }
                                    }
                                    records
                                })
                                .into_iter()
                        },
                    );

                    acc.push(iter);
                }

                acc
            })
            .into_iter();

        IssTraces { traces }
    }
}
