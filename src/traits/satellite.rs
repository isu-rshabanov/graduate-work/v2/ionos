use super::{CelestialLocation, Datetime};

pub trait SatelliteRecord: CelestialLocation + Datetime {}
