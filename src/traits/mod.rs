use crate::types::coords::{CelestialCoords, GeoCoords};

pub mod iss;
pub mod satellite;
pub mod space_weather;

pub trait Datetime {
    fn get_datetime(&self) -> &chrono::NaiveDateTime;
}

pub trait GeoLocation: Datetime {
    fn get_geo_coords(&self) -> &GeoCoords;
}

pub trait CelestialLocation: Datetime {
    fn get_celestial_coords(&self) -> &CelestialCoords;
}
