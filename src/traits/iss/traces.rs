use std::collections::BTreeMap;

use crate::region::Region;

use super::IssRecord;

pub trait Traces<T: IssRecord> {
    fn traces(&self) -> IssTraces<T>;

    fn are_continuous_records(
        &self,
        lhs: &chrono::NaiveDateTime,
        rhs: &chrono::NaiveDateTime,
    ) -> bool {
        lhs.timestamp().abs_diff(rhs.timestamp()) <= 1
    }
}

pub struct IssTraces<'a, T: IssRecord> {
    pub(crate) traces: std::vec::IntoIter<std::vec::IntoIter<&'a T>>,
}

impl<'a, T: IssRecord> Iterator for IssTraces<'a, T> {
    type Item = std::vec::IntoIter<&'a T>;

    fn next(&mut self) -> Option<Self::Item> {
        self.traces.next()
    }
}

pub trait RegionTraces<T: IssRecord>: Traces<T>
where
    T: IssRecord,
{
    fn region_traces(
        &self,
        region: &impl Region,
        kps: &Option<BTreeMap<chrono::NaiveDateTime, f64>>,
        max_kp: f64,
    ) -> IssTraces<T>;
}
