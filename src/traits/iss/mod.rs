use super::{Datetime, GeoLocation};

pub mod traces;

pub trait IssRecord: Datetime + GeoLocation {}
