use crate::types::space_weather::{Bsr, F10_7};

use super::Datetime;

pub trait DayRecord {
    fn get_date(&self) -> chrono::NaiveDate;

    fn get_bsr(&self) -> Bsr;

    fn get_ap(&self) -> i16;

    fn get_sn(&self) -> i16;

    fn get_f10_7(&self) -> F10_7;

    fn get_d(&self) -> i16;
}

pub trait Record3h: Datetime {
    fn get_kp(&self) -> f64;

    fn get_ap(&self) -> i16;

    fn time_range(&self) -> std::ops::Range<chrono::NaiveDateTime> {
        let dt = *self.get_datetime();
        dt..(dt + chrono::Duration::hours(3))
    }
}
