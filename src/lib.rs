use types::coords::GeoCoords;

pub mod earth_consts;
pub mod geodesic;
pub mod implementation;
pub mod region;
pub mod traits;
pub mod types;
pub(crate) mod utils;

pub type Receivers = std::collections::HashMap<String, GeoCoords>;
