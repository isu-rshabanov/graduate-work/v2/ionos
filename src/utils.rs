#[allow(unused)]
pub fn to_kilometers(value: f64) -> f64 {
    value / 1e3
}

#[allow(unused)]
pub fn to_meters(value: f64) -> f64 {
    value * 1e3
}
