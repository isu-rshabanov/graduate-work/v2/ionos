use geographiclib_rs::{DirectGeodesic, Geodesic, InverseGeodesic};

use crate::types::coords::{Altitude, Degree, GeoCoords};

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct GeodesicWrapper {
    geodesic: Geodesic,
}

impl GeodesicWrapper {
    pub fn wgs84() -> Self {
        Self {
            geodesic: Geodesic::wgs84(),
        }
    }

    pub fn new(geodesic: Geodesic) -> Self {
        Self { geodesic }
    }

    /// Creates destination point
    /// alt - altitude (in meters)
    pub fn direct(&self, source: &GeoCoords, azimuth: Degree, alt: Altitude) -> GeoCoords {
        let (lat, lon) = self.geodesic.direct(source.lat, source.lon, azimuth, alt);
        GeoCoords { lat, lon, alt }
    }

    pub fn distance(&self, lhs: &GeoCoords, rhs: &GeoCoords) -> f64 {
        self.geodesic.inverse(lhs.lat, lhs.lon, rhs.lat, rhs.lon)
    }
}
